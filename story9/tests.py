from django.test import TestCase, Client
from django.apps import apps
from .apps import Story9Config
from django.contrib.messages import get_messages

# Create your tests here.
class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')
    def test_create_user(self):
        response = Client().post('/story9/register/', data={'username' : 'tes3', 
            'email' : 'testes@gmail.com', 'password' : 'testing321', 'password2' : 'testing321'}) 
        self.assertEqual(response.status_code, 200)
    def test_create_user_not_post(self):
        response = Client().get('/story9/register/')
        self.assertEqual(response.status_code, 200)

class TestStory9(TestCase):
    def test_html_exists(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index_story9.html')