from django.shortcuts import render, redirect
from django.contrib import messages
from .forms import UserRegisterForm

def index_story9(request):
    return render(request, 'index_story9.html')

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('story9:login')
    else:
        form = UserRegisterForm()
    return render(request, 'register.html', {'form': form})