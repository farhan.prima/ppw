from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

app_name = 'story9'

urlpatterns = [
    path('', views.index_story9, name="index_story9"),
    path('register/', views.register, name="register"),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='logout.html'), name='logout'),
]