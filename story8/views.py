from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index_story8(request):
    return render(request, 'index_story8.html')

def data(request):
    arg = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)