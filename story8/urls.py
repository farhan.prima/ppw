from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.index_story8, name="index_story8"),
    path('data', views.data, name="data"),
]