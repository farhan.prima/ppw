from django.test import TestCase, Client
from django.apps import apps
from .apps import Story8Config

class TestStory8(TestCase):
    def test_html_exists(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index_story8.html')
    def test_request_from_google(self):
        response = Client().get('/story8/data?q=frozen%202')
        self.assertEqual(response.status_code, 200)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')

# Create your tests here.
