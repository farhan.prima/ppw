from django.db import models

class MataKuliah(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    deskripsi = models.TextField()
    semester_tahun = models.CharField(max_length=50)
    ruang_kelas = models.CharField(max_length=50)

class ListMataKuliah(models.Model):
    matkul = MataKuliah
    cnt = models.IntegerField()

class Aktivitas(models.Model):
    nama = models.CharField(max_length=50)

    def __str__(self):
        return self.nama

class Peserta(models.Model):
    nama = models.CharField(max_length=50)
    aktivitas = models.ForeignKey('Aktivitas', on_delete=models.CASCADE)

    def __str__(self):
        return self.nama
