from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver
from . import views
from .forms import MataKuliahDeleteForm
from .models import MataKuliah, Aktivitas, Peserta

# class Testing(TestCase):
#     def setUp(self):
#         matkul = MataKuliah.objects.create(nama="PPW", dosen="Ibu Iis", sks=3, deskripsi="PPW", semester_tahun="Gasal 2020/2021", ruang_kelas="C")
#         matkul2 = MataKuliah.objects.create(nama="PPM", dosen="Ibu Yekti", sks=3, deskripsi="PPM", semester_tahun="Gasal 2020/2021", ruang_kelas="A")

#     def test_home(self):
#         response = Client().get('/')
#         self.assertTemplateUsed(response, "main/home.html")

#     def test_subscribe(self):
#         response = Client().get('/subscribe')
#         self.assertTemplateUsed(response, "main/subscribe.html")

#     def test_story1(self):
#         response = Client().get('/story1')
#         self.assertTemplateUsed(response, "main/story1.html")

#     def test_forms(self):
#         response = Client().get('/forms')
#         self.assertTemplateUsed(response, "main/forms.html")

#     def test_saveforms(self):
#         cnt = MataKuliah.objects.all().count()
#         response = Client().post('/saveforms', data={'nama' : 'SDA', 'dosen' : 'Bu Nilam', 'sks' : 4, 
#         'deskripsi' : 'SDA', 'semester_tahun' : 'Gasal 2020/2021', 'ruang_kelas' : 'G'})
#         self.assertEquals(MataKuliah.objects.all().count(), cnt + 1)
#         self.assertEquals(response.status_code, 302)

#     def test_deleteforms(self):
#         cnt = MataKuliah.objects.all().count()
#         response = Client().post('/deleteforms', data={'nama' : 'PPW', 'sks' : 3})

#         self.assertEquals(MataKuliah.objects.all().count(), cnt - 1)
#         self.assertEquals(response.status_code, 302)

#     def test_detail_matkul(self):
#         cnt = MataKuliah.objects.all().count()
#         list_matkul = MataKuliah.objects.all()
#         response = Client().post('/detail_matkul', data={'nama' : 'PPW', 'sks' : 3})
#         self.assertEquals(cnt, cnt)

#     # doesn't do anything - just to get coverage 100%
#     def test_detail_matkul_wrong(self):
#         cnt = MataKuliah.objects.all().count()
#         list_matkul = MataKuliah.objects.all()
#         response = Client().post('/detail_matkul', data={'nama' : 'SDA', 'sks' : 3})
#         self.assertEquals(response.status_code, 200)

# class Story6Test(TestCase):
#     def setUp(self):
#         aktivitas = Aktivitas.objects.create(nama="Berenang")
#         # peserta = Peserta.objects.create(nama="Adit", aktivitas=aktivitas)

#     def test_check_url(self):
#         response = Client().get('/story6')
#         self.assertEquals(response.status_code, 200)

#     def test_story6_html_loc(self):
#         response = Client().get('/story6')
#         self.assertTemplateUsed(response, "main/story6.html")

#     def test_bikin_objek_aktivitas(self):
#         cnt = Aktivitas.objects.all().count()
#         response = Client().post('/tambah-aktivitas', data={'nama' : 'Berlari'})
#         self.assertEqual(Aktivitas.objects.all().count(), cnt + 1)

    # def test_bikin_objek_peserta(self):
    #     aktivitas2 = Aktivitas.objects.create(nama='Berenang')
    #     peserta2 = Peserta.objects.create(nama='Adit', aktivitas=aktivitas2)
    #     peserta3 = Peserta.objects.create(nama='Budi', aktivitas=aktivitas2)
    #     # cnt = aktivitas2.peserta_set.all().count()

    #     response = Client().post('/tambah-peserta', data={'nama' : 'Charlie', 'aktivitas' : aktivitas2})

    #     self.assertEqual(Peserta.objects.all().count(), 4)

        # self.assertEqual(aktivitas2.peserta_set.all().count(), 3)

        # self.assertEqual(Aktivitas.objects.all().count(), cnt)

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())
