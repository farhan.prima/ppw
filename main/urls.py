from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name="home"),
    path('subscribe', views.subscribe, name="subscribe"),
    path('story1', views.story1, name="story1"),

    path('forms', views.forms, name="forms"),
    path('saveforms', views.saveforms, name="saveforms"),
    path('deleteforms', views.deleteforms, name="deleteforms"),
    path('detail_matkul', views.detail_matkul, name="detail_matkul"),

    # path('story6', views.story6, name="story6"),
    # path('tambah-aktivitas', views.tambah_aktivitas, name="tambahaktivitas"),
    # path('hapus-aktivitas', views.hapus_aktivitas, name="hapusaktivitas"),
    # path('tambah-peserta', views.tambah_peserta, name="tambahpeserta"),
    # path('hapus-peserta', views.hapus_peserta, name="hapuspeserta"),
    
    # path('story6/aktivitas/tambah/', views.tambah_aktivitas, name="story6-tambah-aktivitas"),
    # path('story6/aktivitas/hapus/<int:id>/', views.hapus_aktivitas, name="story6-hapus-aktivitas"),
    # path('story6/peserta/tambah/', views.tambah_peserta, name="story6-tambah-peserta"),
    # path('story6/peserta/hapus/<int:id>/', views.hapus_peserta, name="story6-hapus-peserta"),


]
