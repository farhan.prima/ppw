from django import forms

from .models import MataKuliah, Aktivitas, Peserta

class MataKuliahForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = '__all__'

class MataKuliahDeleteForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ['nama', 'sks']
    nama = forms.CharField(max_length=50)
    sks = forms.IntegerField()

class MataKuliahDetailForm(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = ['nama', 'sks']
    nama = forms.CharField(max_length=50)
    sks = forms.IntegerField()

class TambahAktivitasForm(forms.ModelForm):
    class Meta:
        model = Aktivitas
        fields = '__all__'

class TambahPesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'
