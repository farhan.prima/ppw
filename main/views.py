from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.db import models
from .forms import MataKuliahForm, MataKuliahDeleteForm, MataKuliahDetailForm, TambahAktivitasForm, TambahPesertaForm
from .models import MataKuliah, Aktivitas, Peserta
import random

def home(request):
    return render(request, 'main/home.html')

def subscribe(request):
    return render(request, 'main/subscribe.html')

def story1(request):
    return render(request, 'main/story1.html')

def forms(request):
    response = {'list_matkul' : MataKuliah.objects.all()}
    return render(request, 'main/forms.html', response)

def saveforms(request):
    form = MataKuliahForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return HttpResponseRedirect('/forms')
    # else:
	#     return HttpResponseRedirect('/forms')

def deleteforms(request):
    form = MataKuliahDeleteForm(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        nama = form.cleaned_data['nama']
        sks = form.cleaned_data['sks']
        models = MataKuliah.objects.filter(nama=nama, sks=sks)
        models.delete()

    return HttpResponseRedirect('/forms')
    # else:
    #     return HttpResponseRedirect('/forms')

def detail_matkul(request):
    form = MataKuliahDetailForm(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        response = {'list_matkul' : MataKuliah.objects.all()}

        nama = form.cleaned_data['nama']
        sks = form.cleaned_data['sks']
        try:
            models = MataKuliah.objects.get(nama=nama, sks=sks)
            response['matkul'] = models
        except MataKuliah.DoesNotExist:
            pass
    
    return render(request, 'main/forms.html', response)

def story6(request):
    content = {}
    content['list_aktivitas'] = Aktivitas.objects.all()
    return render(request, 'main/story6.html', content)

def tambah_aktivitas(request):
    form = TambahAktivitasForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return HttpResponseRedirect('/story6')

def tambah_peserta(request):
    form = TambahPesertaForm(request.POST)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return HttpResponseRedirect('/story6')

def hapus_aktivitas(request, id):
    aktivitas = get_object_or_404(Aktivitas, id=id)

    aktivitas.delete()
    return HttpResponseRedirect('/story6')

def hapus_peserta(request, id):
    peserta = get_object_or_404(Peserta, id=id)

    peserta.delete()
    return HttpResponseRedirect('/story6')