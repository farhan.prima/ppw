from django.db import models

class Activity(models.Model):
    name = models.CharField(max_length=50)

class Person(models.Model):
    name = models.CharField(max_length=50)
    activity = models.ForeignKey('Activity', on_delete=models.CASCADE, null=True)
