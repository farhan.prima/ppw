from django.forms import ModelForm
from .models import Activity, Person

class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        fields = '__all__'

class PersonForm(ModelForm):
    class Meta:
        model = Person
        fields = '__all__'