from django.test import TestCase, Client
from .models import Activity, Person

# unittest = komponen dri scope development

class TestStory6(TestCase):
    def setUp(self):
        ac = Activity.objects.create(name='Berenang')
        p = Person.objects.create(name='Budi', activity=ac)
    def test_html_exists(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
    def test_create_activity_object(self):
        cnt = Activity.objects.all().count()
        activity = Activity.objects.create(name='Berlari')
        self.assertEqual(Activity.objects.all().count(), cnt + 1)
    def test_create_activity_object_with_form(self):
        cnt = Activity.objects.all().count()
        response = Client().post('/story6/activity/add/', {'name' : 'Berlari'})
        self.assertEqual(Activity.objects.all().count(), cnt + 1)
    def test_create_person_object(self):
        cnt = Person.objects.all().count()
        person = Person.objects.create(name='Adit')
        self.assertEqual(Person.objects.all().count(), cnt + 1)
    def test_create_person_object_with_form(self):
        cnt = Person.objects.all().count()
        # 1 itu ngarah ke pk 1. Yaitu activity yg dibuat diatas
        response = Client().post('/story6/person/add/', {'name' : 'Adit', 'activity' : '1'})
        self.assertEqual(Person.objects.all().count(), cnt + 1)
    def test_delete_activity_object_with_form(self):
        cnt = Activity.objects.all().count()
        response = Client().post(f'/story6/activity/delete/{Activity.objects.all()[0].id}/')
        self.assertEqual(Activity.objects.all().count(), cnt - 1)
    def test_delete_person_object_with_form(self):
        cnt = Person.objects.all().count()
        response = Client().post(f'/story6/person/delete/{Person.objects.all()[0].id}/')
        self.assertEqual(Person.objects.all().count(), cnt - 1)
        
        