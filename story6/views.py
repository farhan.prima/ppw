from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import ActivityForm, PersonForm
from .models import Activity, Person

def index(request):
    content = {'content' : Activity.objects.all()}
    return render(request, 'index.html', content)
def add_activity(request):
    form = ActivityForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return HttpResponseRedirect('/story6/')
def add_person(request):
    form = PersonForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
    return HttpResponseRedirect('/story6/')
def delete_activity(request, id):
    activity = get_object_or_404(Activity, id=id)
    activity.delete()
    return redirect('story6:index')
def delete_person(request, id):
    person = get_object_or_404(Person, id=id)
    person.delete()
    return redirect('story6:index')
