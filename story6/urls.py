from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name="index"),
    path('activity/add/', views.add_activity, name="add-activity"),
    path('activity/delete/<int:id>/', views.delete_activity, name="delete-activity"),
    path('person/add/', views.add_person, name="add-person"),
    path('person/delete/<int:id>/', views.delete_person, name="delete-person"),
]