// function buka() {
//     $('.answer').show();
// }

// function tutup() {
//     $('.answer').hide();
// }


// Inspired by : https://codepen.io/johnhubler/pen/bGpMYwq

// The event.stopPropagation() stops the click event from bubbling to the parent elements.
$(document).ready(function(){
    $('ul.accordion > li > div.answer').hide();

    $('ul.accordion > li').click(function() {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active").find("div.answer").hide();
        } else {
            $(this).addClass("active").find("div.answer").show();
        }
    });

    $('button.accordion_up, button.accordion_down').click(function(event) {
        event.stopPropagation();
        const btn = $(this);
        const block = btn.parent().parent();
        if (btn.is(".accordion_up")) {
            block.insertBefore(block.prev());
        } else {
            block.insertAfter(block.next());
        }
    });
});

// $(document).ready(function(){
//     $('.accordion-list > li > .answer').hide();

//     $('.accordion-list > li').click(function() {
//         if ($(this).hasClass("active")) {
//             $(this).removeClass("active").find(".answer").slideUp();
//         } else {
//             $(".accordion-list > li.active .answer").slideUp();
//             $(".accordion-list > li.active").removeClass("active");
//             $(this).addClass("active").find(".answer").slideDown();
//         }
//         return false;
//     });

//     });