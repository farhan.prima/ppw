$(document).ready(function() {
    var search_bar = $("#keyword");

    search_bar.keyup(function() {
        var url = "/story8/data?q=" + search_bar.val();
        $.ajax({
            url: url,
            success: function(result) {
                var result_html = $("#result");
                result_html.empty();
                for (i = 0; i < result.items.length; i++) {
                    var title = result.items[i].volumeInfo.title;
                    var image =  result.items[i].volumeInfo.imageLinks.smallThumbnail;
                    result_html.append("<li>" + title + "<br>" 
                        + "<img src=" + image + ">" + "</li>" + "<br>");
                }
            }
        });
    });
});