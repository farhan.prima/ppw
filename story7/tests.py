from django.test import TestCase, Client

class TestStory7(TestCase):
    def test_html_exists(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index_story7.html')

# Create your tests here.
